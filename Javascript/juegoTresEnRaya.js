//Definimos el turno y los puntos de cada jugador
var turno = 0;
var ptsJugador0=0;
var ptsJugador1=0;
//Para cada tirada, aplicamos esta función que nos escribirá una "X" o un "O" si 
//la casilla está vacia. Despues se comprueba si en esa tirada hubo victoria o empate
function tirada(casilla) {
console.log(turno)
if(casilla.innerText == "" ){
    if (turno==0) {
            casilla.innerText = "X";
            compruebaVictoria();
            CompruebaEmpate();
            turno = 1;
    } else{ 
    casilla.innerText = "O";
    compruebaVictoria();
    CompruebaEmpate();
    turno=0; 
    }
}
}
//Comprobamos si alguien a ganado
function compruebaVictoria() {
    //Definimos las combinaciones ganadoras
    combinacionesGanadoras = [[1, 2, 3], [1, 4, 7], [1, 5, 9], [9, 8, 7], [9, 6, 3], [4, 5, 6], [3, 5, 7]];
    combinacionesGanadoras.forEach(comb => {
        if(getText(comb[0])!="" && getText(comb[0])==getText(comb[1]) && getText(comb[2])==getText(comb[1])){
            gana(turno);
        }
    });
    return false;
}
//Funcion con la que comprobamos si se ha producido una vistoria. 
function gana (jugador){
    //Gracias al "turno" y sumando 1, sabemos que jugador ha ganado la partida.
    alert("gano jugador "+ (jugador+1));
    //Al jugador que ha ganado le sumamos los puntos y lo asignamos a la casilla correspondiente
    jugador? document.getElementById("Jugador2").innerHTML=++ptsJugador0:document.getElementById("Jugador1").innerHTML=++ptsJugador1;
    reset();
}
//Función para resetear el tablero
function reset()
{
    turno=0;
    Array.from(document.getElementsByTagName("td")).forEach(i => i.innerText="");

}
//Función para comprobar si se ha producido un empate
function CompruebaEmpate(){
    
    if (($("#1").text() == "X" || $("#1").text() == "O")
     && ($("#2").text() == "X" || $("#2").text() == "O") 
     && ($("#3").text() == "X" || $("#3").text() == "O") 
     && ($("#4").text() == "X" || $("#4").text() == "O") 
     && ($("#5").text() == "X" || $("#5").text() == "O") 
     && ($("#6").text() == "X" || $("#6").text() == "O") 
     && ($("#7").text() == "X" || $("#7").text() == "O") 
     && ($("#8").text() == "X" || $("#8").text() == "O") 
     && ($("#9").text() == "X" || $("#9").text() == "O")) {
        alert("Empate");
        reset();
    }
    
}
function getCell(id){
    return document.getElementById(id);
}
function getText(id){
    var a=getCell(id).innerText
    console.log(id+": "+a);
    
    return a
}